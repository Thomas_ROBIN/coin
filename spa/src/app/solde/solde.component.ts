import { Component, OnInit } from '@angular/core';
import { CoinBaseClass as coinBase } from '../coin-base-class';

@Component({
  selector: 'app-solde',
  templateUrl: './solde.component.html',
  styleUrls: ['./solde.component.scss']
})
export class SoldeComponent implements OnInit {
  coinBase = new coinBase();
  tokenId = "BBB";
  secretToken = "BBB";

  constructor() { }

  ngOnInit() {
    this.tokenId = this.coinBase.getTokenId();
    this.secretToken = this.coinBase.getSecretToken();
  }
}
